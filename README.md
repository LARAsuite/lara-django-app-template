# lara-django-app-template

A template for a LARA python-django app, including templates for setup,
documentation and installer.

1.  Install lara-django

    see Installation section in [lara-django](https://gitlab.com/LARAsuite/lara-django)

2.  generate app

        wget https://gitlab.com/LARAsuite/lara-django-app-template/-/archive/master/lara-django-app-template-master.zip

        django-admin startapp \
        --template lara-django-app-template-master.zip \
        --extension rst-tpl,md-tpl,sh-tpl my_new_app

finally fix some things, django-admin has not done

 cd \[your\_new\_app\_dir\] setup\_app.sh \[your\_new\_app\_name\] 
 # this will rename and delete some files and directories

If required, copy the files/directories of the new app into a new git
repository.

1.  Add "my_new_app" to your INSTALLED\_APPS setting like this:

        INSTALLED_APPS = [
            ...
            'my_new_app',
        ]

2.  Include the lara-django-app URLconf in your project urls.py like
    this:

        path('my_new_app/', include('my_new_app.urls')),

3.  Run python3 manage.py migrate to create the my_new_app database models.
4.  In case you like to test the app, please load the demo data:
5.  Start the development server and visit
    <http://127.0.0.1:8000/admin/> to create some entries (you'll need the
    Admin app enabled).

Environment variables
---------------------

for development, please set :: export
DJANGO\_SETTINGS\_MODULE=lara.settings.devel

for production, please set :: export
DJANGO\_SETTINGS\_MODULE=lara.settings.production

if your media does not reside in the default media folder, please set
environment variable to :: export DJANGO\_MEDIA\_PATH='path/to/my/media'

to use user defined fixtures, please set: :: export
DJANGO\_FIXTURE\_PATH='path/to/user/fixtures'

Installation of required packages
---------------------------------

    pip3 install --user -r requirements/devel.py

### Testing all applications

use this command to run all tests:

    python3 manage.py test

or 

    tox

### testing the application

from the directory where the main lara project is located, type

    ./manage.py test my_new_app

### Generating documentation

To generate the documentation, please change directory to app and run:

    sphinx-apidoc -o docs . cd docs make html

### Acknowledgements

The LARA-django developers thank

     -   the python team
     -   the whole [django](https://www.djangoproject.com/) team  for their framework !

