

creating the package
=====================

.... code-block:: console

    python3 setup.py sdist (run from inside lara-django-app-template)


installing
-----------

.... code-block:: console

    pip3 install --user lara-django-app-template/dist/django-app-template.tar.gz

    # to uninstall the package, use pip3:

    pip3 uninstall lara-django-app-template
